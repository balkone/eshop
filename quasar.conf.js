// Configuration for your app

module.exports = function(ctx) {
    return {
        // app plugins (/src/plugins)
        plugins: ['firebase', 'moment', 'lodash', 'vuelidate', 'axios', 'meli'],
        css: ['app.styl'],
        extras: [
            ctx.theme.mat ? 'roboto-font' : null,
            'material-icons'
        ],
        supportIE: false,
        build: {
            scopeHoisting: true,
            publicPath: 'eShop',
            distDir: '../Okinoi/public/eShop',
            extendWebpack(cfg) {}
        },
        devServer: {
            port: 8090,
            open: true
        },
        framework: {
            i18n: 'es',
            components: [
                'QLayout',
                'QLayoutHeader',
                'QLayoutDrawer',
                'QPageContainer',
                'QPage',
                'QToolbar',
                'QToolbarTitle',
                'QTooltip',
                'QBtn',
                'QIcon',
                'QList',
                'QListHeader',
                'QItem',
                'QItemMain',
                'QItemSide',
                'QItemTile',
                'QItemSeparator',
                'QLayoutFooter',
                'QCollapsible',
                'QSearch',
                'QModal',
                'QInput',
                'QInnerLoading',
                'QCard',
                'QCardTitle',
                'QCardMain',
                'QCardMedia',
                'QCardActions',
                'QCardSeparator',
                'QToggle',
                'QSpinnerDots',
                'QBtnDropdown',
                'QAutocomplete',
                'QTabs',
                'QTab',
                'QTabPane',
                'QTable',
                'QTr',
                'QTd',
                'QChip',
                'QDatetime',
                'QFab',
                'QFabAction',
                'QField',
                'QSelect',
                'QStepper',
                'QStep',
                'QStepperNavigation',
                'QCheckbox',
                'QWindowResizeObservable',
                'QPopupEdit'
            ],
            directives: ['Ripple', 'CloseOverlay'],
            plugins: ['Notify', 'Dialog']
        },
        animations: [],
        ssr: {
            pwa: false
        },
        pwa: {
            // workboxPluginMode: 'InjectManifest',
            // workboxOptions: {},
            manifest: {
                name: 'E-SHOP OKINOI',
                short_name: 'OKN-ESHOP',
                description: 'Aplicación oficial exclusiva para clientes',
                display: 'standalone',
                orientation: 'portrait',
                background_color: '#ffffff',
                theme_color: '#027be3',
                icons: [
                    {
                        src: 'statics/icons/icon-128x128.png',
                        sizes: '128x128',
                        type: 'image/png'
                    },
                    {
                        src: 'statics/icons/icon-192x192.png',
                        sizes: '192x192',
                        type: 'image/png'
                    },
                    {
                        src: 'statics/icons/icon-256x256.png',
                        sizes: '256x256',
                        type: 'image/png'
                    },
                    {
                        src: 'statics/icons/icon-384x384.png',
                        sizes: '384x384',
                        type: 'image/png'
                    },
                    {
                        src: 'statics/icons/icon-512x512.png',
                        sizes: '512x512',
                        type: 'image/png'
                    }
                ]
            }
        },
        cordova: {
            // id: 'org.cordova.quasar.app'
        },
        electron: {
            // bundler: 'builder', // or 'packager'
            extendWebpack(cfg) {
                // do something with Electron process Webpack cfg
            },
            packager: {
                // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options
                // OS X / Mac App Store
                // appBundleId: '',
                // appCategoryType: '',
                // osxSign: '',
                // protocol: 'myapp://path',
                // Window only
                // win32metadata: { ... }
            },
            builder: {
                // https://www.electron.build/configuration/configuration
                // appId: 'quasar-app'
            }
        }
    }
}
