/**
 * Created by german on 20/8/16.
 */

document.addEventListener('DOMContentLoaded', function() {

    var mode = getParameterByName('mode');
    var actionCode = getParameterByName('oobCode');
    var apiKey = getParameterByName('apiKey');

    // Handle the user management action.
    switch (mode) {
        case 'resetPassword':
            // Display reset password handler and UI.
            handleResetPassword(auth, actionCode);
            break;
        case 'recoverEmail':
            // Display email recovery handler and UI.
            handleRecoverEmail(auth, actionCode);
            break;
        case 'verifyEmail':
            // Display email verification handler and UI.
            handleVerifyEmail(auth, actionCode);
            break;
        default:
        // Error: invalid mode.
    }
}, false);

function handleResetPassword(auth, actionCode) {
    var accountEmail;
    // Verify the password reset code is valid.
    auth.verifyPasswordResetCode(actionCode).then(function(email) {
        var accountEmail = email;

        // TODO: Show the reset screen with the user's email and ask the user for
        // the new password.

        // Save the new password.
        auth.confirmPasswordReset(actionCode, newPassword).then(function(resp) {
            // Password reset has been confirmed and new password updated.

            // TODO: Display a link back to the app, or sign-in the user directly
            // if the page belongs to the same domain as the app:
            // auth.signInWithEmailAndPassword(accountEmail, newPassword);
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            var complete = "Error " + errorCode + ": " + errorMessage;
            console.log(complete);
            document.getElementById("body").innerHTML = "<h1>" + complete + "</h1>";
        });
    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        var complete = "Error " + errorCode + ": " + errorMessage;
        console.log(complete);
        document.getElementById("body").innerHTML = "<h1>" + complete + "</h1>";
    });
}

function handleRecoverEmail(auth, actionCode) {
    var restoredEmail = null;
    // Confirm the action code is valid.
    auth.checkActionCode(actionCode).then(function(info) {
        // Get the restored email address.
        restoredEmail = info['data']['email'];

        // Revert to the old email.
        return auth.applyActionCode(actionCode);
    }).then(function() {
        // Account email reverted to restoredEmail

        // TODO: Display a confirmation message to the user.

        // You might also want to give the user the option to reset their password
        // in case the account was compromised:
        auth.sendPasswordResetEmail(restoredEmail).then(function() {
            // Password reset confirmation sent. Ask user to check their email.
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            var complete = "Error " + errorCode + ": " + errorMessage;
            console.log(complete);
            document.getElementById("body").innerHTML = "<h1>" + complete + "</h1>";
        });
    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        var complete = "Error " + errorCode + ": " + errorMessage;
        console.log(complete);
        document.getElementById("body").innerHTML = "<h1>" + complete + "</h1>";
    });
}

function handleVerifyEmail(auth, actionCode) {
    // Try to apply the email verification code.
    auth.applyActionCode(actionCode).then(function(resp) {
        // Email address has been verified.
        window.location.replace('http://okinoi.com.ar/eShop/shop');
    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        var complete = "Error " + errorCode + ": " + errorMessage;
        console.log(complete);
        document.getElementById("body").innerHTML = "<h1>" + complete + "</h1>";
    });
}

function getParameterByName(param) {
    var vars = {};
    window.location.href.replace(location.hash, '').replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function(m, key, value) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );

    if (param) {
        return vars[param] ? vars[param] : null;
    }
    return vars;
}