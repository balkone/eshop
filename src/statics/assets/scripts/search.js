/**
 * Created by german on 20/8/16.
 */

var articulosRef = database.ref('datos/articulos/');

var storageRef = storage.ref('datos/articulos/');

var json = {};

var search_box_text;

var content_box = document.getElementById("products_content");

var json_search = {};

var processing_search = false;

articulosRef.on("child_added", function(snapshot) {
    json[snapshot.key] = snapshot.val().denominacion;
});

function splitText (string) {
    string = string.split(" ");
    var stringArray = new Array();
    for (var i =0; i < string.length; i++) {
        if(string[i] != " " && string[i] != "") {
            stringArray.push(string[i]);
        }
    }

    //console.log(stringArray);

    return stringArray;
}

function searchArticle() {
    var count = Object.keys(json).length;

    if(count == 0) {
        articulosRef.on("child_added", function(snapshot) {
            json[snapshot.key] = snapshot.val().denominacion;
        });
    } else {
        if (!processing_search) {
            processing_search = true;

            updateSearchData();

            var words = splitText(search_box_text);

            var results = [];

            var json_search = {};

            for(var i in words) {
                var word = words[i];

                if (i == 0) {
                    if (!isNaN(parseInt(word))) {
                        for(var index in json) {
                            if (index.indexOf(word) !== -1) {
                                results.push(index);
                            }
                        }
                    } else {
                        for(var index in json) {
                            var article = json[index];

                            if (article != undefined) {
                                if (article.toUpperCase().indexOf(word) !== -1) {
                                    results.push(index);
                                    json_search[index] = article;
                                }
                            }else {
                                console.log(index);
                            }

                        }
                    }
                } else {
                    results = [];
                    var temp_json = {};
                    for(var index in json_search) {
                        var article = json_search[index];

                        if (article.indexOf(word) !== -1) {
                            results.push(index);
                            temp_json[index] = article;
                        }
                    }
                    json_search = temp_json;
                }
            }

            if(results.length > 0) {
                //verProductos();

                createArticleUI(results);
            } else {
                content_box.innerHTML = "No se encontraron resultados";
                processing_search = false;
            }

            processing_search = false;
        }
    }
}

var articles = {};

function createArticleUI(result_id_list) {
    content_box.innerHTML = "";

    for (var i = 0; i < result_id_list.length && i < 30; i++) {
        var img = storage_path + result_id_list[i] + ".jpg?alt=media";

        var html = "<article " + "id='article_" + result_id_list[i] + "' class='productInfo'>";
        html += "<div><div class='cod_producto'>Codigo: " + result_id_list[i] + "</div><div class='contentimg2'>";
        html += "<img onerror='tryUpperExtention(this);' id='img_" + result_id_list[i] + "' alt='sample' src='" + img + "'></div></div>";
        html += "<p id='price_" + result_id_list[i] + "' class='price'>u$s " + "cargando..." + "</p>";
        html += "<p class='productContent'>" + json[result_id_list[i]] + "</p>";
        html += "<button type='button' onclick='agregar_articulo(" + result_id_list[i] + ")' class='buyButton' data-toggle='modal' data-target='#myModal'>Agregar</button></article>";

        content_box.innerHTML += html;

        articulosRef.child(result_id_list[i]).once("value", function(snapshot) {
            articles[result_id_list[i]] = snapshot.val();
            document.getElementById("price_" + result_id_list[i]).innerHTML = "u$s " + snapshot.val()["precio"];
        });

    }

    processing_search = false;
}

var agregar_articulo_destino = "";

function agregar_articulo(articulo_id) {
    switch (agregar_articulo_destino) {
        case "facturacion":
            console.log(agregar_articulo_destino);
			
			database.ref('datos/articulos/' + articulo_id).once('value', function(snap) {
				if(snap.val() != null) {
					
					var obj = snap.val();
					
					obj['observacion'] = "Agregado en facturacion";
					
					obj['cantidad'] = 0;
					
					obj['entregados'] = 0;
					
					lista_pedidos[current_order['id_pedido']]['articulos'][snap.key] = obj;
					
					cargarFactura(current_order['id_pedido']);
				}
			})
            break;
        case "notas_credito":
            console.log(agregar_articulo_destino);

            cargarArticuloEnNotaCredito(articulo_id);
            break;
		case "notas_debito":
            console.log(agregar_articulo_destino);

            cargarArticuloEnNotaDebito(articulo_id);
            break;
    }
}

function loadModalData(key) {
    var article = articles[key];

    var modal_cod = document.getElementById("modal_cod");
    var modal_descripcion = document.getElementById("modal_descripcion");
    var modal_costo = document.getElementById("modal_costo");
    var modal_origen = document.getElementById("modal_origen");
    var modal_img = document.getElementById("modal_img");

    modal_cod.innerHTML = "Codigo: " + article.nro_interno;
    modal_descripcion.innerHTML = " " + article.denominacion;
    modal_origen.innerHTML = " Origen: " + article.origen;
    modal_costo.innerHTML = "U$S " + article.precio;

    modal_img.src = storage_path + article.nro_interno + ".jpg?alt=media";
}

function updateSearchData() {
    if ($(window).width() < 600) {

        var sidebarMenu = document.getElementById("sidebarMenu");
        var sidebarMenuBar = document.getElementById("menubar");
        sidebarMenu.style.cssText = 'display:none !important';
        sidebarMenuBar.style.cssText = 'display:none !important';
    }
    search_box_text = search_box.value.toUpperCase();
}

function downloadJson(json, file_name) {
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(json));
    var dlAnchorElem = document.getElementById('downloadAnchorElem');
    dlAnchorElem.setAttribute("href", dataStr);
    dlAnchorElem.setAttribute("download", file_name + ".json");
    dlAnchorElem.click();
}
