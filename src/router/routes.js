const routes = [
    {
        path: '/login',
        component: () => import('layouts/login.vue'),
        name: 'Login'
    },

    {
        path: '/',
        component: () => import('layouts/default.vue'),
        children: [
            {
                path: '',
                component: () => import('pages/catalogo.vue'),
                name: 'Inicio',
                meta: {
                    title: 'Catálogo de Artículos'
                }
            },
            {
                path: 'agenda',
                component: () => import('pages/agenda.vue'),
                name: 'Agenda',
                meta: {
                    title: 'Agenda de Artículos'
                }
            },
            {
                path: 'catalogo',
                component: () => import('pages/catalogo.vue'),
                name: 'Catalogo',
                meta: {
                    title: 'Catálogo de Artículos'
                }
            },
            {
                path: 'publicar-ml/:codigo',
                component: () => import('pages/publicar-meli.vue'),
                name: 'Publicar Mercado Libre',
                meta: {
                    title: 'Publicar en Mercado Libre'
                }
            },
            {
                path: 'consulta',
                component: () => import('pages/consulta.vue'),
                name: 'Consulta',
                meta: {
                    title: 'Consulta de Pedidos'
                }
            },
            {
                path: 'facturas',
                component: () => import('pages/facturas.vue'),
                name: 'Facturas',
                meta: {
                    title: 'Consulta de Facturas'
                }
            },
            {
                path: 'pedido',
                component: () => import('pages/pedido.vue'),
                name: 'Pedido',
                meta: {
                    title: 'Pedido'
                }
            },
            {
                path: 'ver-pedido/:id',
                component: () => import('pages/ver-pedido.vue'),
                name: 'Ver Pedido',
                meta: {
                    title: 'Ver Pedido'
                }
            },
            {
                path: 'embarques',
                component: () => import('pages/embarques.vue'),
                name: 'Embarques',
                meta: {
                    title: 'Últimos Embarques'
                }
            }
        ]
    }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
    routes.push({
        path: '*',
        component: () => import('pages/Error404.vue')
    })
}

export default routes
