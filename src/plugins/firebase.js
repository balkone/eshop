import firebase from 'firebase/app'
import auth from 'firebase/auth'
import database from 'firebase/database'
import storage from 'firebase/storage'
import firestore from 'firebase/firestore'

import env_prod from 'src/config/env_prod.json'
import env_dev from 'src/config/env_dev.json'

let fireInstance

// singleton
if (!firebase.apps.length) {
    fireInstance = process.env.PROD ? firebase.initializeApp(env_prod) : firebase.initializeApp(env_dev)
} else {
    fireInstance = firebase.app()
}

export const fireApp = fireInstance;

export const AUTH = fireApp.auth()
AUTH.setPersistence(firebase.auth.Auth.Persistence.SESSION)

export const DATABASE = fireApp.database()

export const FIRESTORE = fireApp.firestore()
const settings = { timestampsInSnapshots: true }
FIRESTORE.settings(settings)

export const STORAGE = fireApp.storage()

export default ({ Vue }) => {
    Vue.prototype.$auth = AUTH
    Vue.prototype.$database = DATABASE
    Vue.prototype.$storage = STORAGE
    Vue.prototype.$firestore = FIRESTORE
}
