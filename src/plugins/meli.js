if (process.env.PROD) {
  MELI.init({
    client_id: 1435535282743838,
    xauth_protocol: 'https://',
    xauth_domain: 'secure.mlstatic.com',
    xd_url: '/org-img/sdk/xd-1.0.4.html'
  })
} else {
  MELI.init({
    client_id: 2818993125657687
  })
}


// leave the export, even if you don't use it
export default ({ Vue }) => {

}
