import axios from 'axios'

const origin_url = !process.env.PROD
  ? 'https://okinoi-94a39.firebaseapp.com'
  : 'http://localhost:8080'

axios.defaults.headers = {
  'Access-Control-Allow-Origin': origin_url
}

export default ({ Vue }) => {
  Vue.prototype.$axios = axios
}
