import moment from 'moment'

// leave the export, even if you don't use it
export default ({ Vue }) => {
  Vue.prototype.$moment = moment
}
