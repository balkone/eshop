import Vue from 'vue'
import Vuex from 'vuex'

import main from './main'
import history from './history'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      main,
      history
    }
  })

  return Store
}
