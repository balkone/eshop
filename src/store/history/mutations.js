export function saveSearchTerms(state, terms) {
    state.search.terms = terms
}

export function pageSize(state, userData) {
    state.pageSize = userData.settings && userData.settings.pageSize ? userData.settings.pageSize : 24
}

export function updatePageSize(state, pageSize) {
    state.pageSize = pageSize
}

export function saveSearchResults(state, results) {
    state.search.results = results
}

export function setSearchInput(state, payload) {
    state.searchInput = payload
}
