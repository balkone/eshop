export default {
  pageSize: 24,
  search: {
    terms: '',
    results: []
  },
  searchInput: false
}
