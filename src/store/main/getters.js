export const totalCarrito = state => {
    let total = 0
    if (state.pedido.articulos) {
        state.pedido.articulos.forEach(item => {
            const descuento_e = state.cliente.categoria || 0
            const descuento = descuento_e * item.precio / 100
            const unitario = !item.valor_absoluto ? item.precio - descuento : item.precio
            total = total + (unitario * item.cantidad)
        })
    }

    return total
}

export const itemsSchedule = state => {
    let items = 0
    if (state.agenda.articulos)
        items = state.agenda.articulos.length
    return items
}