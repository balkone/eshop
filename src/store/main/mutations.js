import _ from 'lodash'

export const connectionState = (state, status) => {
    state.connection = status
}

export const setArticulos = (state, articulos) => {
    const ordered = _.orderBy(articulos, ['descripcion'])

    ordered.forEach(item => {
        item.precio = parseFloat(item.precio).toFixed(2)
    })

    state.articulos = articulos
}

export const togglePreciosAr = state => {
    state.preciosAr = !state.preciosAr
}

export const dolar = (state, dolar) => {
    state.dolar = dolar
}

export const articulosPedido = (state, item) => {
    if (!state.pedido.articulos) {
        state.pedido.articulos = []
    }
    state.pedido.articulos.push(item)
}

export const loadOrder = (state, pedido) => {
    state.pedido = pedido
}

export const actualizaArticulos = (state, items) => {
    state.pedido.articulos = []
    state.pedido.articulos = items
}

export const loadSchedule = (state, agenda) => {
    state.agenda = agenda
}

export const clearSchedule = (state) => {
    state.agenda = {
        articulos: []
    }
}

export const addToSchedule = (state, product) => {
    state.agenda.articulos.push(product)
}

export const delScheduleItem = (state, code) => {
    const item = state.agenda.articulos.filter(item => item.codigo == code)[0]
    const index = state.agenda.articulos.indexOf(item)
    state.agenda.articulos.splice(index, 1)
}

export const clearCart = state => {
    state.pedido = {
        articulos: [],
        cliente: '',
        estado: 'Pendiente',
        observacion: '',
        redespacho: '',
        total_carrito: 0,
        transporte: false,
        vendedor: 'Cliente'
    }
}

export const transportes = (state, transportes) => {
    state.transportes = transportes
}

export const usuario = (state, datos) => {
    state.usuario = datos
    if (datos.id) {
      state.pedido.cliente = datos.id.toString()
    }
}

export const filtroConsultaPedidos = (state, filtro) => {
    state.filtroConsultaPedidos = filtro
}

export const version = (state, version) => {
    state.version = version
}

export const resultadoPedidos = (state, resultados) => {
    state.resultadoPedidos = resultados
}

export const resumen = (state, movimientos) => {
    state.resumen = movimientos
}

export const visibleColumns = (state, visibleColumns) => {
    state.visibleColumns = visibleColumns
}

export const vaciarPedido = state => {
    state.pedido.articulos = []
}

export const meliUser = (state, data) => {
    state.meli_user = data
}

export const setTransportePedido = (state, transporte) => {
    state.pedido.transporte = transporte
}

export const setResdespachoPedido = (state, redespacho) => {
    state.pedido.redespacho = redespacho
}

export const redirectUri = (state, redirectUri) => {
    state.redirectUri = redirectUri
}

export const setUserSettings = (state, settings) => {
    state.usuario.settings = settings
}

export const clientData = (state, clientData) => {
    state.cliente = clientData
}

export const rangoConsultaPedidos = (state, dates) => {
    state.rango_consulta_pedidos = dates
}