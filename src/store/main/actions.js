import { DATABASE } from 'src/plugins/firebase'
import axios from 'axios'

export const connectionState = ({ commit }) => {
    const connectedRef = DATABASE.ref('.info/connected')
    connectedRef.on('value', snap => {
        commit('connectionState', snap.val())
    })

}

export const getTransportes = ({ commit }) => {
    DATABASE.ref('datos/search/transportes').on('value', res => {
        const transportesObj = JSON.parse(res.val())
        const transportesArr = []
        for (let x in transportesObj) {
            transportesArr.push({
                value: transportesObj[x],
                label: transportesObj[x]
            })
        }
        commit('transportes', transportesArr)
    })
}

export const getClientData = ({ commit, state }) => {
    const ref = DATABASE.ref('datos/clientes_lite').child(state.usuario.id)
    ref.once('value').then(res => {
        const clientData = res.val()
        clientData.categoria = isNaN(clientData.categoria) ? 0 : clientData.categoria
        commit('clientData', clientData)
    })
}

export const articulos = ({ commit }) => {
    const localArticulos = localStorage.getItem('articulos')
    if (localArticulos) {
        commit('setArticulos', JSON.parse(localStorage.getItem('articulos')))
    }

    DATABASE
        .ref('datos/articulos')
        .orderByChild('lista')
        .equalTo('1')
        .once('value')
        .then(res => {
            const results = res.val()
            const articulos = []

            for (let index in results) {
                const articulo = results[index]
                articulos.push({
                    codigo: index,
                    descripcion: articulo.denominacion,
                    precio: articulo.precio,
                    valor_absoluto: articulo.aplica_dto_e != 'Si'
                })
            }
            localStorage.setItem('articulos', JSON.stringify(articulos))
            commit('setArticulos', articulos)
        })
}

export const loadOrder = ({ commit, state }) => {
    DATABASE.ref('datos/usuarios/' + state.usuario.key + '/temp')
        .once('value')
        .then(res => {
            const pedidoTemp = res.val()
            if (pedidoTemp) {
                if (!pedidoTemp.articulos) {
                    pedidoTemp.articulos = []
                }
                commit('loadOrder', pedidoTemp)
            } else {
                commit('clearCart')
            }
        })
}

export const loadSchedule = ({commit, state}) => {
    DATABASE.ref(`datos/usuarios/${state.usuario.key}/agenda`).once('value').then(res => {
        const schedule = res.val()
        if (schedule) {
            commit('loadSchedule', schedule)
        } else {
            commit('clearSchedule')
        }
    })
}

export const getMeliToken = ({ commit, state }, payload) => {
    const app_id = state.meli.app_id
    const secret_key = state.meli.secret_key
    const auth_code = payload.auth_code
    const redirect_uri = state.meli.base_url

    const uri = `https://api.mercadolibre.com/oauth/token?grant_type=authorization_code&client_id=${app_id}&client_secret=${secret_key}&code=${auth_code}&redirect_uri=${redirect_uri}`

    axios.post(uri).then(res => {
        if (res.data) {
            const record = res.data
            commit('meliToken', record)

            record.expiration = new Date().getTime() + 18000000 // fecha de expiración AHORA + 5 HORAS
            window.localStorage.setItem('meli_token', JSON.stringify(record))
        }
    })
}
