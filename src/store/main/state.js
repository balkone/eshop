export default {
    version: '',
    connection: true,
    articulos: [],
    cliente: {},
    preciosAr: false,
    dolar: 0,
    pedido: {},
    agenda: {
        articulos: []
    },
    visibleColumns: ['fecha', '$id', 'razonSocial', 'estado', 'totalCarrito', 'totalOrder'],
    resultadoPedidos: [],
    resumen: [],
    usuario: {
        settings: {}
    },
    transportes: [],
    meli_user: {},
    rango_consulta_pedidos: {}
}
